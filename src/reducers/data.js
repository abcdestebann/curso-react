import dataApi from '../schemas/index';
import { fromJS } from 'immutable';

const initialState = fromJS({
	entities: dataApi.entities,
	categories: dataApi.result.categories,
	search: ''
})

/**
 *
 *
 * @export
 * @param {*} state
 * @param {*} action
 * @returns
 */
export default function data(state = initialState, action) {
	switch (action.type) {
		case 'SEARCH_VIDEO':
			if (action.data.query) return state.set('search', action.data.query)
			else return state
		default:
			return state
	}
}
