import React from 'react'
import './Controls.css'

const Controls = (props) => {
   const { children } = props
   return (
      <div className="VideoPlayerControls">
         {children}
      </div>
   )
}

export default Controls