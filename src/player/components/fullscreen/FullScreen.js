import React from 'react'
import Screen from '../../../components/icons/FullScreen'
import './FullScreen.css'

const FullScreen = (props) => (
   <div className="FullScreen" onClick={props.handleFullScreen}>
      <Screen color="#ff476a" fontSize="40px" display="flex" justifyContent="center" alignItems="center" />
   </div>
)

export default FullScreen