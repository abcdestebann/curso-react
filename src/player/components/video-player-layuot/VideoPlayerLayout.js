import React from 'react'
import './VideoPlayerLayout.css'

const VideoPlayerLayout = (props) => (
   <div ref={props.setRef} className="VideoPlayerLayout">
      {props.children}
   </div>
)

export default VideoPlayerLayout