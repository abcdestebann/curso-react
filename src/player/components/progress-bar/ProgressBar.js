import React from 'react'
import './ProgressBar.css'

const ProgressBar = (props) => {
   return (
      <div className="ProgressBar">
         <input type="range"
            value={props.min}
            max={props.max}
            onChange={props.handleProgressChange}
         />
      </div>
   )
}

export default ProgressBar