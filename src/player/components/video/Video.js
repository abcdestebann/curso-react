import React, { Component } from 'react';
import './Video.css'


export class Video extends Component {

   tooglePlay = () => {
      if (this.props.pause) this.video.play()
      else this.video.pause()
   }

   componentWillReceiveProps(nextProps) {
      if (nextProps.pause !== this.props.pause) {
         this.tooglePlay()
      }
   }

   setRef = element => (this.video = element)

   render() {
      const { handleLoadedMetaData, autoplay, src, handleTimeUpdate, handleOnSeeking, handleOnSeeked } = this.props
      return (
         <div className="Video">
            <div className="Video-Title">
               <h2>{this.props.title}</h2>
            </div>
            <video
               ref={this.setRef}
               autoPlay={autoplay}
               src={src}
               onLoadedMetadata={handleLoadedMetaData}
               onTimeUpdate={handleTimeUpdate}
               onSeeking={handleOnSeeking}
               onSeeked={handleOnSeeked} />
         </div>
      )
   }
};
