import React from 'react';
import VolumeIcon from '../../../components/icons/Volume'
import Mute from '../../../components/icons/Mute'
import './Volume.css'

const Volume = (props) => {
   return (
      <button className="Volume">
         <div onClick={props.handleToggleVolume}>
            {
               props.mute
                  ? <Mute color="#ff476a" fontSize="30px" display="flex" justifyContent="center" alignItems="center" />
                  : <VolumeIcon color="#ff476a" fontSize="30px" display="flex" justifyContent="center" alignItems="center" />
            }
         </div>
         <div className="Volume-range">
            <input type="range" min={0} max={1} step={.05} onChange={props.handleVolumeChange} />
         </div>
      </button>

   )
}

export default Volume