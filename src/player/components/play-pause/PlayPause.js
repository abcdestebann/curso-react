import React from 'react'
import Play from '../../../components/icons/Play'
import Pause from '../../../components/icons/Pause'
import './PlayPause.css'

const PlayPause = (props) => (
   <div className="PlayPause">
      {
         props.pause
            ? <button onClick={props.handleTogglePlayPause}> <Play color="#ff476a" fontSize="40px" display="flex"
               justifyContent="center" alignItems="center" /> </button>
            : <button onClick={props.handleTogglePlayPause}> <Pause color="#ff476a" fontSize="40px" display="flex"
               justifyContent="center" alignItems="center" /></button>
      }
   </div>
)

export default PlayPause