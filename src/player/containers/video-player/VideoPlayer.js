import React, { Component } from 'react'
import VideoPlayerLayout from '../../components/video-player-layuot/VideoPlayerLayout';
import { Video } from '../../components/video/Video'
import PlayPause from '../../components/play-pause/PlayPause';
import Timer from '../../components/timer/Timer';
import Controls from '../../components/controls/Controls'
import ProgressBar from '../../components/progress-bar/ProgressBar';
import Spinner from '../../components/spinner/Spinner';
import Volume from '../../components/volume/Volume'
import FullScreen from '../../components/fullscreen/FullScreen'
import { connect } from 'react-redux';
class VideoPlayer extends Component {
   state = {
      pause: true,
      duration: 0,
      currentTime: 0,
      loading: false,
      currentVolumen: 1,
      mute: false
   }

   handleTogglePlayPause = () => {
      this.setState({
         pause: !this.state.pause
      })
   }

   componentDidMount() {
      this.setState({
         pause: (!this.props.autoplay) ? true : false
      })
   }

   handleLoadedMetaData = event => {
      this.video = event.target;
      this.setState({
         duration: this.video.duration
      })
   }

   handleTimeUpdate = event => {
      this.setState({
         currentTime: this.video.currentTime
      })
   }

   handleProgressChange = event => {
      const { value } = event.target
      this.video.currentTime = value
      this.setState({
         currentTime: value
      })
   }

   handleOnSeeking = event => {
      this.setState({
         loading: true
      })
   }
   handleOnSeeked = event => {
      this.setState({
         loading: false
      })
   }

   handleVolumeChange = event => {
      const { value } = event.target
      this.video.volume = value
      this.setState({ currentVolumen: value })
   }

   handleToggleVolume = () => {
      if (this.state.mute) {
         this.setState({ mute: false })
         this.video.volume = this.state.currentVolumen
      } else {
         this.setState({ mute: true })
         this.video.volume = 0
      }
   }

   handleFullScreen = (event) => {
      if (!document.webkitIsFullScreen) {
         console.log('Chao')
         this.video.webkitRequestFullScreen()
      } else {
         document.webkitExitFullscreen()
         console.log('Hola')
      }
   }

   setRefVideoPlayer = element => {
      this.player = element
   }

   render() {
      return (
         <VideoPlayerLayout setRef={this.setRefVideoPlayer}>
            <Controls>
               <PlayPause pause={this.state.pause} handleTogglePlayPause={this.handleTogglePlayPause} />
               <Timer duration={this.state.duration} currentTime={this.state.currentTime} />
               <ProgressBar min={this.state.currentTime} max={this.state.duration} handleProgressChange={this.handleProgressChange} />
               <Volume mute={this.state.mute} handleVolumeChange={this.handleVolumeChange} handleToggleVolume={this.handleToggleVolume} />
               <FullScreen handleFullScreen={this.handleFullScreen} />
            </Controls>
            <Spinner active={this.state.loading} />
            <Video
               title={this.props.media.get('title')}
               autoplay={this.props.autoplay}
               src={this.props.media.get('src')}
               pause={this.state.pause}
               handleLoadedMetaData={this.handleLoadedMetaData}
               handleTimeUpdate={this.handleTimeUpdate}
               handleOnSeeking={this.handleOnSeeking}
               handleOnSeeked={this.handleOnSeeked}
            />
         </VideoPlayerLayout>
      )
   }
}

const mapStateToProps = (state, ownProps) => {
   return {
      media: state.get('data').get('entities').get('media').get(ownProps.id)
   }
}

export default connect(mapStateToProps)(VideoPlayer)