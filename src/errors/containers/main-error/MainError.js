import React, { Component } from 'react'
import RegularrError from '../../components/regular-error/RegularError';
export class MainError extends Component {

   state = {
      handleError: false
   }

   componentDidCatch(error, info) {
      this.setState({
         handleError: true
      })
   }

   render() {
      if (this.state.handleError) {
         return <RegularrError />
      }
      return this.props.children
   }
}



