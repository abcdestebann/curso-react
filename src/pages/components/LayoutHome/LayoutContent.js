import React from 'react';
import './LayoutHome.css'
export default function LayoutContent(props) {
   return (
      <section className="content">
         {props.children}
      </section>
   )
}