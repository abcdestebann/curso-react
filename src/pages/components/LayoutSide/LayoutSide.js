
import React from 'react'
import './LayoutSide.css'

export default function LayoutSide(props) {
   return (
      <div className="side">
         <img src="../images/logo.png" alt="logo" width={250} />
      </div>
   )
}