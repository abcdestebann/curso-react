import React, { Component } from 'react'

// REDUX
import { connect } from 'react-redux'

// LAYOUTS
import LayoutHome from '../../components/LayoutHome/LayoutContent'
import LayoutSide from '../../components/LayoutSide/LayoutSide';

// COMPONENTS
import Categories from '../../../components/categories/Categories';
import Modal from '../../../components/modal/Modal'
import { MainError } from '../../../errors/containers/main-error/MainError'
import { ModalContainer } from '../../../components/modal-container/ModalContainer';
import Search from '../../../components/search/Search'
import VideoPlayer from '../../../player/containers/video-player/VideoPlayer';

import { List as list } from 'immutable';

// STYLES
import './Home.css'

class Home extends Component {

	handleToggleModal = (id) => {
		this.props.dispatch({
			type: 'CLOSE_MODAL',
			payload: {
				mediaId: id
			}
		})
	}

	render() {
		return (
			<MainError>
				<LayoutHome>
					<LayoutSide />
					<div style={{ backgroundColor: 'white' }}>
						<Search />
						<Categories
							handleToggleModal={this.handleToggleModal}
							categories={this.props.categories}
							search={this.props.search} />
					</div>
					{
						this.props.modalVisible.get('visibility') && <ModalContainer>
							<Modal id={this.props.modalVisible.get('mediaId')} handleToggleModal={this.handleToggleModal}>
								<VideoPlayer id={this.props.modalVisible.get('mediaId')} autoplay={true} />
							</Modal>
						</ModalContainer>
					}
				</LayoutHome>
			</MainError>
		)
	}
}

map

const mapStateToProps = (state, props) => {
	const categories = state.get('data').get('categories').map((categoryId) => state.get('data').get('entities').get('categories').get(categoryId))
	let searchResults = list()
	const search = state.get('data').get('search')
	if (search) {
		const mediaList = state.get('data').get('entities').get('media')
		searchResults = mediaList.filter((media) => (
			media.get('author').toLowerCase().includes(search.toLowerCase()) || media.get('title').toLowerCase().includes(search.toLowerCase())
		)).toList()
	}
	return {
		categories: categories,
		search: searchResults,
		modalVisible: state.get('modal')
	}
}

export default connect(mapStateToProps)(Home)