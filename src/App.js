import React, { Component } from 'react';
import './App.css';

// DATA
// import data from './api.json';
import data from './schemas/index'

// PAGES 
import Home from './pages/containers/home/Home';

// REDUX
import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import reducer from './reducers/index'
import { Map as map } from 'immutable';

import logger from 'redux-logger'
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk'
// const logger = ({ dispatch, getState }) => {
//   return (next) => {
//     return (action) => {
//       console.log('vamos a enviar esta accion', action)
//       const value = next(action)
//       console.log('este es mi nuevo estado', getState().toJS())
//       return value
//     }
//   }
// }

const store = createStore(
  reducer,
  map(),
  composeWithDevTools(applyMiddleware(logger, thunk))
)

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Home />
      </Provider>
    );
  }
}

export default App;
