import data from '../api.json'
import { normalize, schema } from 'normalizr'

const media = new schema.Entity('media', {}, {
   idAttribute: 'id',
   processStrategy: (value, parent, id) => ({ ...value, categoryId: parent.id })
})

const category = new schema.Entity('categories', {
   playlist: new schema.Array(media)
})

const categories = { categories: new schema.Array(category) }

const normalizedData = normalize(data, categories)

export default normalizedData