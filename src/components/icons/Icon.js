import React from 'react';

export default function Icon(props) {
   const style = props
   return (
      <div style={style}>
         {props.children}
      </div>
   )
}
