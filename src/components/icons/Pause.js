import React from 'react';
import Icon from './Icon'

export default function Pause(props) {
   const style = {
      fontSize: 'inherit'
   }
   return (
      <Icon {...props}>
         <i style={style} className="material-icons"> pause </i>
      </Icon>
   )
}
