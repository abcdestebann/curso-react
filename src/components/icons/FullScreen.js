import React from 'react';
import Icon from './Icon'

export default function FullScreen(props) {
   const style = {
      fontSize: 'inherit'
   }
   return (
      <Icon {...props}>
         <i style={style} className="material-icons"> fullscreen_exit </i>
      </Icon>
   )
}
