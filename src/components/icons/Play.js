import React from 'react';
import Icon from './Icon'

export default function Play(props) {
   const style = {
      fontSize: 'inherit'
   }
   return (
      <Icon {...props}>
         <i style={style} className="material-icons"> play_arrow </i>
      </Icon>
   )
}
