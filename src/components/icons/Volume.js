import React from 'react';
import Icon from './Icon'

export default function Volume(props) {
   const style = {
      fontSize: 'inherit'
   }
   return (
      <Icon {...props}>
         <i style={style} className="material-icons"> volume_up </i>
      </Icon>
   )
}
