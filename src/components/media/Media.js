import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import './Media.css'

export class Media extends Component {

   constructor(props) {
      super(props)
      this.state = {
         image: this.props.cover,
         title: this.props.title,
         author: this.props.author
      }
   }

   // CICLO DE VIDA

   componentWillMount() { } // Se ejecuta antes de montar el componente // Se podría usar para hacer un setState()

   //render() { } // Contiene todos los elementos a renderizar // podrías usarlo para calcular propiedades (ej: concatenar una cadena)

   componentDidMount() { }//Solo se lanza una vez //Ideal para llamar a una API, hacer un setInteval, etc

   //Actualización:
   componentWillReceiveProps() { } //Es llamado cuando el componente recibe nuevas propiedades.

   // shouldComponentUpdate() { } //Idea para poner una condición y  si las propiedades que le llegaron anteriormente
   // eran las mismas que tenia retornar false para evitar re-renderear el componente

   componentWillUpdate() { }//metodo llamado antes de re-renderizar el componente si shouldComponentUpdate devolvió true
   // re-render si es necesario...

   componentDidUpdate() { } //Método llamado luego del re-render

   componentWillUnmount() { } //Método llamado antes de desmontar el componente

   componentDidCatch() { } // Si ocurre algún error, lo capturo desde acá: 


   handelClick = (event) => {
      this.props.handleToggleModal(this.props.id)
   }

   render() {
      return (
         <div className="div-card">
            <Card className="card" onClick={this.handelClick} >
               <CardMedia
                  className="media"
                  image={this.state.image}
                  title=""
               />
               <CardContent>
                  <h4 className="media-title">{this.state.title}</h4>
                  <p className="media-author">{this.state.author}</p>
               </CardContent>
            </Card>
         </div>
      );
   }
}

Media.propTypes = {
   cover: PropTypes.string.isRequired,
   title: PropTypes.string.isRequired,
   author: PropTypes.string.isRequired
}
