import React, { Component } from 'react';
import { Media } from '../media/Media';
import { connect } from 'react-redux'


class MediaContainer extends Component {
   handleToggleModal = (id) => {
      this.props.dispatch({
         type: 'OPEN_MODAL',
         payload: {
            mediaId: id
         }
      })
   }

   render() {
      return (
         <Media {...this.props.data.toJS()} handleToggleModal={this.handleToggleModal} />
      )
   }
};

const mapStateToProps = (state, props) => {
   return {
      data: state.get('data').get('entities').get('media').get(props.id),
      modal: state.get('modal')
   }
}

export default connect(mapStateToProps)(MediaContainer)