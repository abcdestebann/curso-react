import React, { Component } from 'react'
import TextField from '@material-ui/core/TextField';
import { connect } from 'react-redux'
class Search extends Component {

   state = {
      valueInput: ''
   }

   handleSearch = (event) => {
      event.preventDefault()
      this.props.dispatch({
         type: 'SEARCH_VIDEO',
         data: {
            query: this.input.value
         }
      })
   }

   setValueInput = (element) => {
      this.input = element
   }

   render() {
      return (
         <form style={{ padding: ' 0% 2%' }} noValidate autoComplete="off">
            <TextField
               id="search"
               label="Busca tu video favorito"
               style={{ width: 500 }}
               margin="dense"
               onKeyUp={this.handleSearch}
               inputRef={this.setValueInput}
            />
         </form>
      )
   }
}

export default connect()(Search)