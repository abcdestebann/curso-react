import React from 'react'
import { Media } from '../media/Media'
import './Playlist.css';
import MediaContainer from '../media-container/MediaContainer';

export default function Playlist(props) {
   return (
      <div className="playlist">
         {
            props.playlist.map((mediaId) => <MediaContainer key={mediaId} id={mediaId} handleToggleModal={props.handleToggleModal} />)
         }
      </div>
   )
}

