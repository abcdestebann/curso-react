import React from 'react'
import './Modal.css'

const handleClick = (props) => {
   props.handleToggleModal(props.id)
}

export default function Modal(props) {
   return (
      <div className="Modal">
         {props.children}
         <button className="Modal-close" onClick={() => { handleClick(props) }} />
      </div>
   )
}
