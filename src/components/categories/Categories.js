import React from 'react'
import Category from '../category/Category';
import { Media } from '../media/Media'
import './Categories.css'
export default function Categories(props) {

	return (

		<div className="categories">
			{
				props.search.map((item) => {
					return <Media {...item.toJS()} key={item.get('id')} handleToggleModal={props.handleToggleModal} />
				})
			}
			{
				props.categories.map((category) => <Category
					key={category.get('id')}
					{...category.toJS()}
					handleToggleModal={props.handleToggleModal} />)
			}
		</div>
	)
}