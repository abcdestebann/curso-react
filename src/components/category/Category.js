import React from 'react';
import Playlist from '../playlist/Playlist';
import './Category.css'

export default function Category(props) {
   const category = {
      id: props.id,
      description: props.description,
      title: props.title,
      playlist: props.playlist
   }
   return (
      <div className="category">
         <span className="category-description"> {category.description} </span>
         <span className="category-title"> {category.title} </span>
         <Playlist key={category.id} playlist={category.playlist} handleToggleModal={props.handleToggleModal} />
      </div>
   )
}